
<?php
session_start();
require_once 'class.user.php';

$reg_user = new USER();

if(!$reg_user->is_logged_in()!="")
{
	$reg_user->redirect('home.php');
}


if(isset($_POST['btn-apply']))
{	
	//$loantype = $_POST['txtLoanRequest'];
	$loanrequest = $_POST['txtLoanRequest'];
	$employer = $_POST['txtEmployer'];
	$krapin = $_POST['txtkrapin'];
	$income = $_POST['txtincome'];
	$guarantor1 = $_POST['txtguarantor1'];
	$guarantor2 = $_POST['txtguarantor2'];
	$othersecurity = $_POST['txtotherdocument'];
	$email = $_POST['txtemail'];
	$userfullname = $_POST['txtuserfullname'];
	$userpaymentmethod = $_POST['txtpaymentmethod'];
	
	//$stmt = $reg_user->runQuery("SELECT * FROM tbl_users WHERE userEmail=:email_id");
	//$stmt->execute(array(":email_id"=>$email));
	//$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
	if($reg_user->LoanApplication ( $loanrequest, $employer, $krapin, $income, $guarantor1,  $guarantor2, $othersecurity, $email, $userpaymentmethod))//$loantype,
	
		{			
			
			$loanofficermessage = "					
						Dear Sir/Madam,
						<br><br>
						<br>
						Kindly review the loan application.<br>
						<br><br>						
						<b>Loan Details:</b><br>
						<br><br>
						<b>Loan Request:</b> $loanrequest<br>
						<b>Employer:</b> $employer<br>
						<b>KRA Pin:</b> $krapin<br>
						<b>Income:</b> $income<br>
						<b>Guarantor 1:</b> $guarantor1<br>
						<b>Guarantor 2:</b> $guarantor2<br>
						<b>Other Document:</b> $othersecurity<br>
						<b>Other Document:</b> $userpaymentmethod<br>
						<br><br>
						<b><a href='http://localhost:8083/email/loan_officer_verification.php?Email=$email'>Click HERE to Verify</a></b>
						<br><br>
						Thanks,";
						
			$subject = "Loan Application for $userfullname";
			
			 
			$reg_user->send_mail('officer@loans.com',$loanofficermessage,$subject);	
			$msg = "
					<div class='alert alert-success'>
						<button class='close' data-dismiss='alert'>&times;</button>
						<strong>Success!</strong>  We've sent an email to officer@loans.com.
					Please click on the confirmation link in the email to create your account. 
					</div>
					";
					//echo "$key=$value<br />"; 
			
						
			
		}
		else
		{
			echo "sorry , Query could no execute...";
		}
}

		$stmt = $reg_user->runQuery("SELECT * FROM tbl_users WHERE userID=:uid");
		$stmt->execute(array(":uid"=>$_SESSION['userSession']));
		$row = $stmt->fetch(PDO::FETCH_ASSOC);

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Loan Form</title>
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
    <link href="assets/styles.css" rel="stylesheet" media="screen">
     <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
	
  </head>
  <body id="login">
    <div class="container">
	
	<div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Member Home</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> 
								<?php echo $row['userEmail']; ?> <i class="caret"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!--<ul class="nav">
                            <li class="active">
                                <a href="http://www.codingcage.com/">Coding Cage</a>
                            </li>
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">Tutorials <b class="caret"></b>

                                </a>
                                <ul class="dropdown-menu" id="menu1">
                                    <li><a href="http://www.codingcage.com/search/label/PHP OOP">PHP OOP</a></li>
                                    <li><a href="http://www.codingcage.com/search/label/PDO">PHP PDO</a></li>
                                    <li><a href="http://www.codingcage.com/search/label/jQuery">jQuery</a></li>
                                    <li><a href="http://www.codingcage.com/search/label/Bootstrap">Bootstrap</a></li>
                                    <li><a href="http://www.codingcage.com/search/label/CRUD">CRUD</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="http://www.codingcage.com/2015/09/login-registration-email-verification-forgot-password-php.html">Tutorial Link</a>
                            </li>
                            
                            
                        </ul>-->
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
	
				<?php if(isset($msg)) echo $msg;  ?>
      <form class="form-signin" method="post">
        <h2 class="form-signin-heading">Loan Form</h2><hr />
        <input type="text" id="loanrequest" class="input-block-level" placeholder="Loan Request" name="txtLoanRequest" required />
		<div class="error" style="display:none">Loan Request cannot have value greater than Loan Limit</div>
        <input type="text" class="input-block-level" placeholder="Employer" name="txtEmployer" required />
		<input type="text" class="input-block-level" placeholder="Krapin" name="txtkrapin" required />
        <input type="text" class="input-block-level" placeholder="Income" name="txtincome" required />
		<input type="text" class="input-block-level" placeholder="Guarantor1" name="txtguarantor1" required />
        <input type="text" class="input-block-level" placeholder="Guarantor2" name="txtguarantor2" required />
        <input type="text" class="input-block-level" placeholder="Otherdocument" name="txtotherdocument" required />
		<input type="text" class="input-block-level" placeholder="Payment Method" name="txtpaymentmethod" required />		
		<input type="text" id="loanlimit" class="input-block-level" placeholder="Loan Limit" name="txtloanlimit" value="<?php echo $row['loanLimit']; ?>" readonly />
		<div class="error" style="display:none">Loan Limit Value</div>
		<input type="text" class="input-block-level" placeholder="Email" name="txtemail" value="<?php echo $row['userEmail']; ?>" readonly />
		<input type="hidden" class="input-block-level" placeholder="Email" name="txtuserfullname" value="<?php echo $row['userFullName']; ?>" readonly />
     	<hr />
        <button id="submit" class="btn btn-large btn-primary" type="submit" name="btn-apply">Apply</button>
		<a href="user_dashboard.php" style="float:right;" class="btn btn-large">Home</a>
      </form>

    </div> <!-- /container -->
    <script src="bootstrap/js/jquery-1.9.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/scripts.js"></script>
	<script>
		$("#loanrequest").focusout(function(){
		
			
			if(parseFloat($("#loanlimit").val()) < parseFloat($("#loanrequest").val()))
			{
				$(".error").css("display","block").css("color","red");
				$("#submit").prop('disabled',true);
			}
			else {
				$(".error").css("display","none");
				$("#submit").prop('disabled',false);        
			}
			
		});
	</script>
  </body>
</html>


