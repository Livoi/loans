<?php
session_start();
require_once 'class.user.php';

$user_signup = new USER();

if($user_signup->is_logged_in()!="")
{
	$user_signup->redirect('index.php');
}


if(isset($_POST['btn-signup']))
{
	$uname = trim($_POST['txtuname']);
	$email = trim($_POST['txtemail']);
	$upass = trim($_POST['txtpass']);
	$fullname = trim($_POST['txtfullname']);
	$age = trim($_POST['txtage']);
	$address = trim($_POST['txtaddress']);
	$contact = trim($_POST['txtcontact']);
	$code = md5(uniqid(rand()));
	
	$stmt = $user_signup->runQuery("SELECT * FROM tbl_users WHERE userEmail=:email_id");
	$stmt->execute(array(":email_id"=>$email));
	$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
	if($stmt->rowCount() > 0)
	{
		$msg = "
		      <div class='alert alert-error'>
				<button class='close' data-dismiss='alert'>&times;</button>
					<strong>Sorry !</strong>  email allready exists , Please Try another one
			  </div>
			  ";
	}
	else
	{
		if($user_signup->register($uname,$email,$upass,$code,$fullname,$age,$address,$contact))
		{			
			$id = $user_signup->lasdID();		
			$key = base64_encode($id);
			$id = $key;
			
			$usermessage = "					
						Hello <b>$uname</b>,
						<br><br>
						Welcome to Coding Cage!<br>
						To complete your registration  please , just click following link<br>
						<br><br>
						<b>User Deatils:</b><br>
						<br><br>
						<b>Full Name:</b> $fullname<br>
						<b>Age:</b> $age<br>
						<b>Address:</b> $address<br>
						<b>Contact:</b> $contact<br>
						<br><br>
						<b><a href='http://localhost:8083/email/verify.php?id=$id&code=$code'>Click HERE to Activate</a></b>
						<br><br>
						Thanks,";
						
						
						$loanofficermessage = "					
						Dear Sir/Madam,
						<br><br>
						<br>
						Kindly verify the user below , just click following link<br>
						<br><br>
						<b>User Deatils:</b><br>
						<br><br>
						<b>Full Name:</b> $fullname<br>
						<b>Age:</b> $age<br>
						<b>Address:</b> $address<br>
						<b>Contact:</b> $contact<br>
						<br><br>
						<b><a href='http://localhost:8083/email/loan_officer_verification.php?Email=$email'>Click HERE to Verify</a></b>
						<br><br>
						Thanks,";
						
			$subject = "Confirm Registration";
			
			$personal_details=array($email => $usermessage, "officer@loans.com" => $loanofficermessage);  
			foreach ( $personal_details as $key => $value )  
			{  
				$user_signup->send_mail($key,$value,$subject);	
				$msg = "
						<div class='alert alert-success'>
							<button class='close' data-dismiss='alert'>&times;</button>
							<strong>Success!</strong>  We've sent an email to $email.
						Please click on the confirmation link in the email to create your account. 
						</div>
						";
						//echo "$key=$value<br />"; 
			} 
						
			
		}
		else
		{
			echo "sorry , Query could no execute...";
		}		
	}
}
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Signup | Coding Cage</title>
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
    <link href="assets/styles.css" rel="stylesheet" media="screen">
     <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
  </head>
  <body id="login">
    <div class="container">
	
				<?php if(isset($msg)) echo $msg;  ?>
      <form class="form-signin" method="post">
        <h2 class="form-signin-heading">Sign Up</h2><hr />
        <input type="text" class="input-block-level" placeholder="Username" name="txtuname" required />
        <input type="email" class="input-block-level" placeholder="Email address" name="txtemail" required />
		<input type="text" class="input-block-level" placeholder="FullName" name="txtfullname" required />
        <input type="text" class="input-block-level" placeholder="Age" name="txtage" required />
		<input type="text" class="input-block-level" placeholder="Contact" name="txtcontact" required />
        <input type="text" class="input-block-level" placeholder="Address" name="txtaddress" required />
        <input type="password" class="input-block-level" placeholder="Password" name="txtpass" required />
     	<hr />
        <button class="btn btn-large btn-primary" type="submit" name="btn-signup">Sign Up</button>
        <a href="index.php" style="float:right;" class="btn btn-large">Sign In</a>
      </form>

    </div> <!-- /container -->
    <script src="bootstrap/js/jquery-1.9.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/scripts.js"></script>
  </body>
</html>