<?php

require_once 'dbconfig.php';

class USER
{	

	private $conn;
	
	public function __construct()
	{
		$database = new Database();
		$db = $database->dbConnection();
		$this->conn = $db;
    }
	
	public function runQuery($sql)
	{
		$stmt = $this->conn->prepare($sql);
		return $stmt;
	}
	
	public function lasdID()
	{
		$stmt = $this->conn->lastInsertId();
		return $stmt;
	}
	
	public function register($uname,$email,$upass,$code,$fullname,$age,$address,$contact)
	{
		try
		{							
			$password = md5($upass);
			$stmt = $this->conn->prepare("INSERT INTO tbl_users(userName,userEmail,userPass,tokenCode,userFullName,userAge,userAddress,userContact) 
			                                             VALUES(:user_name, :user_mail, :user_pass, :active_code, :user_fullname, :user_age, :user_address, :user_contact)");
			$stmt->bindparam(":user_name",$uname);
			$stmt->bindparam(":user_mail",$email);
			$stmt->bindparam(":user_pass",$password);
			$stmt->bindparam(":active_code",$code);
			$stmt->bindparam(":user_fullname",$fullname);
			$stmt->bindparam(":user_age",$age);
			$stmt->bindparam(":user_address",$address);
			$stmt->bindparam(":user_contact",$contact);
			$stmt->execute();	
			return $stmt;
		}
		catch(PDOException $ex)
		{
			echo $ex->getMessage();
		}
	}
	
		public function LoanApplication( $loanrequest, $employer, $krapin, $income, $guarantor1,  $guarantor2, $othersecurity, $email, $userpaymentmethod)//$loantype,
	{
		try
		{							
			//$password = md5($upass);
			//loantype,
			$stmt = $this->conn->prepare("INSERT INTO loans(loanrequest,employer,krapin,income,guarantor1,guarantor2,othersecurity,userEmail, paymentMethod)
			                            VALUES( :user_loanrequest, :user_employer, :user_krapin, :user_income, :user_guarantor1, :user_guarantor2, :user_othersecurity, :user_email, :user_paymentmethod)");
			//:user_loantype,
			//$stmt->bindparam(":user_loantype",$loantype);
			$stmt->bindparam(":user_loanrequest",$loanrequest);
			$stmt->bindparam(":user_employer",$employer);
			$stmt->bindparam(":user_krapin",$krapin);
			$stmt->bindparam(":user_income",$income);
			$stmt->bindparam(":user_guarantor1",$guarantor1);
			$stmt->bindparam(":user_guarantor2",$guarantor2);
			$stmt->bindparam(":user_othersecurity",$othersecurity);
			$stmt->bindparam(":user_email",$email);
			$stmt->bindparam(":user_paymentmethod",$userpaymentmethod);
			$stmt->execute();	
			return $stmt;
		}
		catch(PDOException $ex)
		{
			echo $ex->getMessage();
		}
	}
	
	public function acc_verification($uname,$email,$verification,$loanLimit,$fullname,$age,$address,$contact)
	{
		try
		{							
			//echo "ok";
			$stmt = $this->conn->prepare("UPDATE tbl_users set userVerification= :user_verification, loanLimit=:user_loanlimit WHERE userEmail=:user_mail");
			$stmt->bindparam(":user_verification",$verification);
			$stmt->bindparam(":user_mail",$email);
			$stmt->bindparam(":user_loanlimit",$loanLimit);
			$stmt->execute();	
			return $stmt;
		}
		catch(PDOException $ex)
		{
			echo $ex->getMessage();
		}
	}
	
	public function LoanApplicationReview($loanstatus, $paydate, $uniqueid)
	{
		try
		{							
			//echo "ok";
			$stmt = $this->conn->prepare("UPDATE loans set loanStatus= :loanstatus, payDate=:user_paydate WHERE uniqueID=:user_uniqueid");
			$stmt->bindparam(":loanstatus",$loanstatus);
			$stmt->bindparam(":user_paydate",$paydate);
			$stmt->bindparam(":user_uniqueid",$uniqueid);
			$stmt->execute();	
			return $stmt;
		}
		catch(PDOException $ex)
		{
			echo $ex->getMessage();
		}
	}
	
	public function login($email,$upass)
	{
		try
		{
			$stmt = $this->conn->prepare("SELECT * FROM tbl_users WHERE userEmail=:email_id");
			$stmt->execute(array(":email_id"=>$email));
			$userRow=$stmt->fetch(PDO::FETCH_ASSOC);
			
			if($stmt->rowCount() == 1)
			{
				if($userRow['userStatus']=="Y")
				{
					if($userRow['userPass']==md5($upass))
					{
						$_SESSION['userSession'] = $userRow['userID'];
						return true;
					}
					else
					{
						header("Location: index.php?error");
						exit;
					}
				}
				else
				{
					header("Location: index.php?inactive");
					exit;
				}	
			}
			else
			{
				header("Location: index.php?error");
				exit;
			}		
		}
		catch(PDOException $ex)
		{
			echo $ex->getMessage();
		}
	}
	
	
	public function is_logged_in()
	{
		if(isset($_SESSION['userSession']))
		{
			return true;
		}
	}
	
	public function redirect($url)
	{
		header("Location: $url");
	}
	
	public function logout()
	{
		session_destroy();
		$_SESSION['userSession'] = false;
	}
	
	function send_mail($email,$message,$subject)
	{						
		/*require_once('mailer/class.phpmailer.php');
		$mail = new PHPMailer();
		$mail->IsSMTP(); 
		$mail->SMTPDebug  = 0;                     
		$mail->SMTPAuth   = true;                  
		$mail->SMTPSecure = "ssl";                 
		$mail->Host       = "192.168.0.100";      
		$mail->Port       = 465;             
		$mail->AddAddress($email);
		$mail->Username="demo@camunda,com";  
		$mail->Password="Password01";            
		$mail->SetFrom('your_gmail_id_here@gmail.com','Coding Cage');
		$mail->AddReplyTo("your_gmail_id_here@gmail.com","Coding Cage");
		$mail->Subject    = $subject;
		$mail->MsgHTML($message);
		$mail->Send();*/
		$to = $email;
		$subjects = $subject;
		$messages = $message;
		$headers = 'From: support@activelab.dev' . "\r\n" .
		'Reply-To: support@activelab.dev' . "\r\n" .
		'X-Mailer: PHP/' . phpversion();
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
		mail($to, $subjects, $messages, $headers);
	}
}