<?php
session_start();
require_once 'db.php';
require_once 'class.user.php';
$user_home = new USER();

if(!$user_home->is_logged_in())
{
	$user_home->redirect('index.php');
}

$stmt = $user_home->runQuery("SELECT * FROM tbl_users WHERE userID=:uid");
$stmt->execute(array(":uid"=>$_SESSION['userSession']));
$row = $stmt->fetch(PDO::FETCH_ASSOC);

$email = $row['userEmail'];

$query="SELECT * FROM loans WHERE userEmail = '$email'";
$result = $mysqli->query($query) or die($mysqli->error.__LINE__);

?>

<!DOCTYPE html>
<html class="no-js">
    
    <head>
        <title><?php echo $row['userEmail']; ?></title>
        <!-- Bootstrap -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
        <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
        <link href="assets/styles.css" rel="stylesheet" media="screen">
        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        
    </head>
    
    <body>
        <div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Member Home</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> 
								<?php echo $row['userEmail']; ?> <i class="caret"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!--<ul class="nav">
                            <li class="active">
                                <a href="http://www.codingcage.com/">Coding Cage</a>
                            </li>
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">Tutorials <b class="caret"></b>

                                </a>
                                <ul class="dropdown-menu" id="menu1">
                                    <li><a href="http://www.codingcage.com/search/label/PHP OOP">PHP OOP</a></li>
                                    <li><a href="http://www.codingcage.com/search/label/PDO">PHP PDO</a></li>
                                    <li><a href="http://www.codingcage.com/search/label/jQuery">jQuery</a></li>
                                    <li><a href="http://www.codingcage.com/search/label/Bootstrap">Bootstrap</a></li>
                                    <li><a href="http://www.codingcage.com/search/label/CRUD">CRUD</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="http://www.codingcage.com/2015/09/login-registration-email-verification-forgot-password-php.html">Tutorial Link</a>
                            </li>
                            
                            
                        </ul>-->
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
		<div class="container"><br>
			<a href="loanapplication.php" style="float:left;" class="btn btn-large">Apply</a><br><br><br>
			<div class="panel-heading"><strong><span class="glyphicon glyphicon-th"></span> Loan Application List</strong></div><br>
			<table class="table table-striped table-bordered">
						<thead>
							<tr>
								<th>Loan Request</th>
								<th>Loan Type</th>
								<th>Employer</th>
								<th>Kra Pin</th>
								<th>Income</th>
								<th>Guarantor 1</th>
								<th>Guarantor 2</th>
								<th>Other Security</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
						<?php
							$i=0;
							while($row = mysqli_fetch_array($result)) {
							if($i%2==0)
							$classname="evenRow";
							else
							$classname="oddRow";
							?>
							<tr>
								<td><?php echo $row["loanRequest"]; ?></td>
								<td><?php echo $row["loanType"]; ?></td>
								<td><?php echo $row["employer"]; ?></td>
								<td><?php echo $row["krapin"]; ?></td>
								<td><?php echo $row["income"]; ?></td>
								<td><?php echo $row["guarantor1"]; ?></td>
								<td><?php echo $row["guarantor2"]; ?></td>
								<td><?php echo $row["otherSecurity"]; ?></td>
								<td><?php echo $row["loanStatus"]; ?></td>
							</tr>
							<?php
								$i++;
								}
								?>
						</tbody>
					</table>
				</div>
        <!--/.fluid-container-->
        <script src="bootstrap/js/jquery-1.9.1.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/scripts.js"></script>
        
    </body>

</html>