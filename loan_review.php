
<?php
session_start();
require_once 'db.php';
require_once 'class.user.php';

$reg_user = new USER();

if(!$reg_user->is_logged_in()!="")
{
	$reg_user->redirect('home.php');
}

$status = '%';
if(isset($_GET['uniqueid'])){
$uniqueId = $_GET['uniqueid'];
}


if(isset($_POST['btn-apply']))
{	
	$uniqueid = $_POST['uniqueID'];
	$loanstatus = $_POST['LoanStatusCH'];
	$paydate = $_POST['payDate'];
	$loanrequest = $_POST['txtLoanRequest'];
	$employer = $_POST['txtEmployer'];
	$krapin = $_POST['txtkrapin'];
	$income = $_POST['txtincome'];
	$guarantor1 = $_POST['txtguarantor1'];
	$guarantor2 = $_POST['txtguarantor2'];
	$othersecurity = $_POST['txtotherdocument'];
	$email = $_POST['txtemail'];
	
	//$stmt = $reg_user->runQuery("SELECT * FROM tbl_users WHERE userEmail=:email_id");
	//$stmt->execute(array(":email_id"=>$email));
	//$row = $stmt->fetch(PDO::FETCH_ASSOC);
	
	if($reg_user->LoanApplicationReview ( $loanstatus, $paydate, $uniqueid,$loanrequest, $employer, $krapin, $income, $guarantor1,  $guarantor2, $othersecurity, $email))//$loantype,
	
		{			
			
			$loanofficermessage = "					
						Dear Sir/Madam,
						<br><br>
						<br>
						Loan application Status: $loanstatus.<br>
						<br><br>						
						<b>Loan Details:</b><br>
						<br><br>
						<b>Loan Request:</b> $loanrequest<br>
						<b>Employer:</b> $employer<br>
						<b>KRA Pin:</b> $krapin<br>
						<b>Income:</b> $income<br>
						<b>Guarantor 1:</b> $guarantor1<br>
						<b>Guarantor 2:</b> $guarantor2<br>
						<b>Other Document:</b> $othersecurity<br>
						<br><br>
						
						<br><br>
						Thanks,";
						
			$subject = "Loan Application Status";
			
			 
			$reg_user->send_mail($email,$loanofficermessage,$subject);	
			$msg = "
					<div class='alert alert-success'>
						<button class='close' data-dismiss='alert'>&times;</button>
						<strong>Success!</strong>  We've sent an email to $email.
					Please click on the confirmation link in the email to create your account. 
					</div>
					";
					//echo "$key=$value<br />"; 
			
						
			
		}
		else
		{
			echo "sorry , Query could no execute...";
		}
}

		$stmt = $reg_user->runQuery("SELECT * FROM tbl_users WHERE userID=:uid");
		$stmt->execute(array(":uid"=>$_SESSION['userSession']));
		$rows = $stmt->fetch(PDO::FETCH_ASSOC);
		
		//$email = $rows['userEmail'];
		
		$query="SELECT * FROM loans WHERE uniqueID = $uniqueId";
		$result = $mysqli->query($query) or die($mysqli->error.__LINE__);

		if($result->num_rows > 0) {
			$row = $result->fetch_array();

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Loan Form</title>
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
    <link href="assets/styles.css" rel="stylesheet" media="screen">
     <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
	<script type="text/javascript">

		function yesnoCheck() {
			if (document.getElementById('yesCheck').checked) {
				document.getElementById('ifYes').style.display = 'block';
				document.getElementById('ifYes1').style.display = 'block';
			}
			else document.getElementById('ifYes').style.display = 'none';
			document.getElementById('ifYes1').style.display = 'none';

		}

	</script>
  </head>
  <body id="login">
    <div class="container">
	
	<div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Member Home</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> 
								<?php echo $row['userEmail']; ?> <i class="caret"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!--<ul class="nav">
                            <li class="active">
                                <a href="http://www.codingcage.com/">Coding Cage</a>
                            </li>
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">Tutorials <b class="caret"></b>

                                </a>
                                <ul class="dropdown-menu" id="menu1">
                                    <li><a href="http://www.codingcage.com/search/label/PHP OOP">PHP OOP</a></li>
                                    <li><a href="http://www.codingcage.com/search/label/PDO">PHP PDO</a></li>
                                    <li><a href="http://www.codingcage.com/search/label/jQuery">jQuery</a></li>
                                    <li><a href="http://www.codingcage.com/search/label/Bootstrap">Bootstrap</a></li>
                                    <li><a href="http://www.codingcage.com/search/label/CRUD">CRUD</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="http://www.codingcage.com/2015/09/login-registration-email-verification-forgot-password-php.html">Tutorial Link</a>
                            </li>
                            
                            
                        </ul>-->
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
	
				<?php if(isset($msg)) echo $msg;  ?>
      <form class="form-signin" method="post">
        <h2 class="form-signin-heading">Loan Form</h2><hr />
		<input type="text" class="input-block-level" placeholder="Loan Request" name="txtLoanRequest" value="<?php echo $row["loanType"]; ?>" readonly />
        <input type="text" class="input-block-level" placeholder="Employer" name="txtEmployer" value="<?php echo $row["employer"]; ?>" readonly />
		<input type="text" class="input-block-level" placeholder="Krapin" name="txtkrapin" value="<?php echo $row["krapin"]; ?>" readonly />
        <input type="text" class="input-block-level" placeholder="Income" name="txtincome" value="<?php echo $row["income"]; ?>" readonly />
		<input type="text" class="input-block-level" placeholder="Guarantor1" name="txtguarantor1" value="<?php echo $row["guarantor1"]; ?>" readonly />
        <input type="text" class="input-block-level" placeholder="Guarantor2" name="txtguarantor2" value="<?php echo $row["guarantor2"]; ?>" readonly />
        <input type="text" class="input-block-level" placeholder="Otherdocument" name="txtotherdocument" value="<?php echo $row["otherSecurity"]; ?>" readonly />
		<input type="text" class="input-block-level" placeholder="Payment Method" name="txtpaymentmethod" value="<?php echo $row["paymentMethod"]; ?>" readonly />
		<input type="text" class="input-block-level" placeholder="Email" name="txtemail" value="<?php echo $row["userEmail"]; ?>" readonly />
		<input type="hidden" class="input-block-level" placeholder="Email" name="uniqueID" value="<?php echo $row["uniqueID"]; ?>" readonly />
		Approved <input type="radio" onclick="javascript:yesnoCheck();" name="LoanStatusCH" id="yesCheck" value="Approved"> 
		Declined <input type="radio" onclick="javascript:yesnoCheck();" name="LoanStatusCH" id="noCheck" value="Declined" /><br><br>
		<div id='ifYes' style="display:none"> 
		<input type='text'  class="input-block-level" placeholder="Pay Date" name='payDate'>
		<label>(yyyy-mm-dd)</label></div>
     	<hr />
        <button class="btn btn-large btn-primary" type="submit" name="btn-apply">Apply</button>
		<a href="loan_officer_dashboard.php" style="float:right;" class="btn btn-large">Home</a>
      </form>
<?php } ?>
    </div> <!-- /container -->
    <script src="bootstrap/js/jquery-1.9.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/scripts.js"></script>
  </body>
</html>


