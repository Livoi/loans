<?php 
session_start();
require_once 'db.php';
require_once 'class.user.php';

$verify_user = new USER();

if(!$verify_user->is_logged_in()!="")
{
	$verify_user->redirect('index.php');
}
 // The mysql database connection script
$status = '%';
if(isset($_GET['Email'])){
$email = $_GET['Email'];
}

if(count($_POST)>0)
{
	$uname = trim($_POST['txtuname']);
	$email = trim($_POST['txtemail']);
	$fullname = trim($_POST['txtfullname']);
	$age = trim($_POST['txtage']);
	$address = trim($_POST['txtaddress']);
	$contact = trim($_POST['txtcontact']);
	$verification = trim($_POST['verificationCH']);
	$loanLimit = trim($_POST['intloanLimit']);
	$code = md5(uniqid(rand()));
	
	
		if($verify_user->acc_verification($uname,$email,$verification,$loanLimit,$fullname,$age,$address,$contact))
		{			
						
			$usermessage = "					
						Hello <b>$uname</b>,
						<br><br>
						Welcome to Loan System!<br>
						Account Status : $verification. Below are your details;<br>
						<br><br>
						<b>User Details:</b><br>
						<br><br>
						<b>Full Name:</b> $fullname<br>
						<b>Age:</b> $age<br>
						<b>Address:</b> $address<br>
						<b>Contact:</b> $contact<br>
						<br><br>
						<br><br>
						Thanks,";
						
			$subject = "Account Activation Status";
			
		
				$verify_user->send_mail($email,$usermessage,$subject);	
				$msg = "
						<div class='alert alert-success'>
							<button class='close' data-dismiss='alert'>&times;</button>
							<strong>Success!</strong>  We've sent an email to $email. 
						</div>
						";
						//echo "$key=$value<br />"; 
						
			
		}
		else
		{
			echo "sorry , Query could no execute...";
		}
	
}

$query="SELECT * FROM tbl_users WHERE userEmail = '$email'";
$result = $mysqli->query($query) or die($mysqli->error.__LINE__);

if($result->num_rows > 0) {
	$row = $result->fetch_array();
	//echo $json_response = json_encode($row);
	//echo "$row";

  ?>

<!DOCTYPE html>
<html>
  <head>
    <title>Loan Applicant Verification</title>
    <!-- Bootstrap -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <link href="bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" media="screen">
    <link href="assets/styles.css" rel="stylesheet" media="screen">
     <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
	<script type="text/javascript">

		function yesnoCheck() {
			if (document.getElementById('yesCheck').checked) {
				document.getElementById('ifYes').style.display = 'block';
			}
			else document.getElementById('ifYes').style.display = 'none';

		}

	</script>
  </head>
  <body id="login">
    <div class="container">
	
	<div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="#">Member Home</a>
                    <div class="nav-collapse collapse">
                        <ul class="nav pull-right">
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i> 
								<?php echo $row['userEmail']; ?> <i class="caret"></i>
                                </a>
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="logout.php">Logout</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <!--<ul class="nav">
                            <li class="active">
                                <a href="http://www.codingcage.com/">Coding Cage</a>
                            </li>
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">Tutorials <b class="caret"></b>

                                </a>
                                <ul class="dropdown-menu" id="menu1">
                                    <li><a href="http://www.codingcage.com/search/label/PHP OOP">PHP OOP</a></li>
                                    <li><a href="http://www.codingcage.com/search/label/PDO">PHP PDO</a></li>
                                    <li><a href="http://www.codingcage.com/search/label/jQuery">jQuery</a></li>
                                    <li><a href="http://www.codingcage.com/search/label/Bootstrap">Bootstrap</a></li>
                                    <li><a href="http://www.codingcage.com/search/label/CRUD">CRUD</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="http://www.codingcage.com/2015/09/login-registration-email-verification-forgot-password-php.html">Tutorial Link</a>
                            </li>
                            
                            
                        </ul>-->
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
        </div>
		
	<?php if(isset($msg)) echo $msg;  ?>
								

      <form class="form-signin" method="post">

        <h2 class="form-signin-heading">Loan Applicant Verification</h2><hr />
        <input type="text" class="input-block-level" placeholder="Username" name="txtuname" value="<?php echo $row['userName']?>" readonly />
        <input type="email" class="input-block-level" placeholder="Email address" name="txtemail" value="<?php echo $row['userEmail']?>" readonly />
		<input type="text" class="input-block-level" placeholder="FullName" name="txtfullname" value="<?php echo $row['userFullName']?>" readonly />
        <input type="text" class="input-block-level" placeholder="Age" name="txtage" value="<?php echo $row['userAge']?>" readonly />
		<input type="text" class="input-block-level" placeholder="Contact" name="txtcontact" value="<?php echo $row['userContact']?>" readonly />
        <input type="text" class="input-block-level" placeholder="Address" name="txtaddress" value="<?php echo $row['userAddress']?>" readonly />
		Verified <input type="radio" onclick="javascript:yesnoCheck();" name="verificationCH" id="yesCheck" value="Verified"> 
		Not Verified <input type="radio" onclick="javascript:yesnoCheck();" name="verificationCH" id="noCheck" value="Not Verified" /><br><br>
		<input type='text' id='ifYes' style="display:none" class="input-block-level" placeholder="Loan Limit" name='intloanLimit'>
     	<hr />
        <button class="btn btn-large btn-primary" type="submit" name="btn-verify">Submit</button>
		<a href="loan_verification_dashboard.php" style="float:right;" class="btn btn-large">Home</a>
      </form>
<?php } ?>
    </div> <!-- /container -->
    <script src="bootstrap/js/jquery-1.9.1.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/scripts.js"></script>
  </body>
</html>